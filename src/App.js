import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { BalanceSummary, NewBudgetForm, NewSpendingForm } from "./components";

import "./App.scss";
import "./assets/scss/main.scss";

function App() {
  const [budgetAmount, handlerBudgetAmount] = useState(0);
  const [remainingAmount, handlerRemainingAmount] = useState(0);
  const [salaryAmount, handlerSalaryAmount] = useState(0);
  const [spendingAmount, setSpendingsAmount] = useState({spendTitle: '', spendAmount: ''});
  const [setSpendings, handlerSetSpendings] = useState(false);


  return (
    <Container fluid>
      <Row>
        <Col
          xs={12}
          className="d-flex justify-content-center align-items-center pt-5 mb-2"
        >
          <h1 className="main-title comic-neue-bold">My Budget</h1>
        </Col>
      </Row>
      {setSpendings ? (
        <Container>
          <Row>
            <Col xs={12} md={6}>
              <Row>
                <Col xs={12} className="mb-4">
                  <BalanceSummary budget={budgetAmount} salary={salaryAmount} remainingAmount={remainingAmount} />
                </Col>
                <Col xs={12}>
                  <NewSpendingForm spendingAmount={spendingAmount} setSpendingsAmount={setSpendingsAmount} />
                </Col>
              </Row>
            </Col>
            <Col xs={12} md={6}>
              <h1 className="main-title comic-neue-bold">Set new spending</h1>
            </Col>
          </Row>
        </Container>
      ) : (
        <Row>
          <Col
            xs={12}
            className="d-flex flex-column justify-content-center align-items-center p-1"
          >
            <NewBudgetForm
              handlerBudgetAmount={handlerBudgetAmount}
              handlerRemainingAmount={handlerRemainingAmount}
              handlerSalaryAmount={handlerSalaryAmount}
              handlerSetSpendings={handlerSetSpendings}
            />
          </Col>
        </Row>
      )}
    </Container>
  );
}

export default App;
