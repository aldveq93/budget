import React from 'react';
import {Form, FormControl, InputGroup ,Button} from 'react-bootstrap';

function NewSpendingForm({spendingAmount, setSpendingsAmount}) {


    const {spendTitle, spendAmount} = spendingAmount;

    const getNewSpendingData = e => {
        setSpendingsAmount({
            ...spendingAmount,
            [e.target.name]: e.target.value
        });
    }

    const validateNewSpending = e => {
        e.preventDefault();

    }

    return (  
        <Form className="new-budget-form p-4 px-md-5 py-md-4 boder-radius-18" onClick={validateNewSpending}>
            <h3 className="comic-neue-bold color-white text-center new-budget-form__title">New Spending</h3>
            
            <Form.Label className="color-white lato-regular new-budget-form__form-label">Title</Form.Label>
            <Form.Control 
                type="text" 
                className="new-budget-form__form-input mb-4" 
                name="spending-title" 
                defaultValue={spendTitle}
                onChange={getNewSpendingData}
                placeholder="Enter spending title" 
            />
            
            <Form.Label className="color-white lato-regular new-budget-form__form-label">Amount</Form.Label>
            <InputGroup className="new-budget-form__input-group w-100 mb-5">
                <InputGroup.Prepend>
                <InputGroup.Text className="new-budget-form__addon">$</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    type="number"
                    className="new-budget-form__form-input"
                    name="spending-amount"
                    onChange={getNewSpendingData}
                    defaultValue={spendAmount}
                    placeholder="Enter spending amount"
                />
            </InputGroup>

            <Button variant="primary" type="submit" className="color-white w-100 lato-regular new-budget-form__form-button mb-3">
                Add new spending
            </Button>
        </Form>
    );
}

export default NewSpendingForm;