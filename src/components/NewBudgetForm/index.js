import React, { useState } from "react";
import { Form, FormControl, InputGroup, Button } from "react-bootstrap";
import Feedback from "../Feedback";
import "./NewBudgetForm.scss";

function NewBudgetForm({
  handlerBudgetAmount,
  handlerRemainingAmount,
  handlerSalaryAmount,
  handlerSetSpendings,
}) {
  // State of the form
  const [userData, handleUserData] = useState({
    budget: 0,
    salary: 0,
  });

  // State of feedback
  const [feedback, handleFeedback] = useState({
    feedbackBy: "",
    feedbackBool: false,
  });

  // Getting data from the form and setting up the state
  const getUserData = (e) => {
    handleUserData({
      ...userData,
      [e.target.name]: parseInt(e.target.value),
    });
  };

  // Getting the data from the form state
  const { budget, salary } = userData;

  const submitUserData = (e) => {
    e.preventDefault();

    if (budget <= 0 && salary <= 0) {
      handleFeedback({
        feedbackBy: "both",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else if (budget <= 0 || isNaN(budget)) {
      handleFeedback({
        feedbackBy: "budget",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else if (salary <= 0 || isNaN(salary)) {
      handleFeedback({
        feedbackBy: "salary",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else if (
      (budget <= 0 || isNaN(budget)) &&
      (salary <= 0 || isNaN(salary))
    ) {
      handleFeedback({
        feedbackBy: "both",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else if (
      (budget === 0 && salary === 0) ||
      (isNaN(budget) && isNaN(salary))
    ) {
      handleFeedback({
        feedbackBy: "both",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else if (salary < budget) {
      handleFeedback({
        feedbackBy: "salary",
        feedbackBool: true,
      });
      setTimeout(setFeedbackFalse, 7000);
    } else {
      handlerBudgetAmount(budget);
      handlerRemainingAmount(budget);
      handlerSalaryAmount(salary);
      handlerSetSpendings(true);
    }
  };

  const setFeedbackFalse = () => {
    handleFeedback({
      feedbackBy: "",
      feedbackBool: false,
    });
  };

  return (
    <>
      {feedback.feedbackBy === "budget" && feedback.feedbackBool === true ? (
        <Feedback
          feedbackType="danger"
          feedbackMessage="The budget amount must be greather than zero, could not be negative and should be a number."
          setFeedbackFalse={setFeedbackFalse}
        />
      ) : feedback.feedbackBy === "salary" && feedback.feedbackBool === true ? (
        <Feedback
          feedbackType="danger"
          feedbackMessage="The salary amount must be greather than zero and greather than the budget, could not be negative and should be a number."
          setFeedbackFalse={setFeedbackFalse}
        />
      ) : feedback.feedbackBy === "both" && feedback.feedbackBool === true ? (
        <Feedback
          feedbackType="danger"
          feedbackMessage="The budget/salary amount must be greather than zero, could not be negative and should be numbers."
          setFeedbackFalse={setFeedbackFalse}
        />
      ) : null}

      <Form
        className="new-budget-form p-4 px-md-5 py-md-4 boder-radius-18"
        onSubmit={submitUserData}
      >
        <h3 className="comic-neue-bold color-white text-center new-budget-form__title">
          New Budget
        </h3>

        <Form.Label className="color-white lato-regular new-budget-form__form-label">
          Budget
        </Form.Label>
        <InputGroup className="new-budget-form__input-group mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text className="new-budget-form__addon">
              $
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="number"
            className="new-budget-form__form-input"
            placeholder="Enter budget"
            name="budget"
            aria-label=""
            aria-describedby=""
            onChange={getUserData}
          />
        </InputGroup>

        <Form.Label className="color-white lato-regular new-budget-form__form-label">
          Salary
        </Form.Label>
        <InputGroup className="new-budget-form__input-group mb-5">
          <InputGroup.Prepend>
            <InputGroup.Text className="new-budget-form__addon">
              $
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            type="number"
            className="new-budget-form__form-input"
            placeholder="Enter salary"
            name="salary"
            aria-label=""
            aria-describedby=""
            onChange={getUserData}
          />
        </InputGroup>

        <Button
          variant="primary"
          type="submit"
          className="color-white w-100 lato-regular new-budget-form__form-button mb-3"
        >
          Add new budget
        </Button>
      </Form>
    </>
  );
}

export default NewBudgetForm;
