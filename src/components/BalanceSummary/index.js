import React from 'react';
import {Card, ListGroup} from 'react-bootstrap';
import './BalanceSummary.scss';

const dateInstance = new Date();
const monthIndex = dateInstance.getMonth();
const currentYear = dateInstance.getFullYear();
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];


const BalanceSummary = ({budget, salary, remaining}) => (
    <Card className="balance-summary-container boder-radius-18">
        <ListGroup variant="flush">
            <ListGroup.Item className="balance-summary-container__list-item bg-transparent d-flex justify-content-between align-items-center py-4">
                <span className="color-white comic-neue-bold balance-summary-container__balance-title">Balance</span>
                <span className="color-white balance-summary-container__date lato-regular">{`${(months[monthIndex]).toUpperCase()}, ${currentYear}`}</span>
            </ListGroup.Item>
            <ListGroup.Item className="balance-summary-container__list-item bg-transparent d-flex justify-content-between py-4">
                <div className="d-flex justify-content-between align-items-center">
                    <span className="balance-summary-container__budget">${budget}</span>
                    <span className="balance-summary-container__budget-state bg-success"></span>
                </div>
                <div className="d-flex justify-content-center align-items-center">
                    <p className="text-white mb-0">hello</p>
                </div>
            </ListGroup.Item>
            <ListGroup.Item className="balance-summary-container__list-item bg-transparent d-flex justify-content-between py-4">
                <div className="d-flex justify-content-between align-items-center">
                    <span className="balance-summary-container__salary"><span className="color-white mr-2 comic-neue-bold">Salary:</span>${salary}</span>
                </div>
                <div className="d-flex justify-content-center align-items-center">
                    <span className="balance-summary-container__salary"><span className="color-white mr-2 comic-neue-bold">Spend:</span>$0</span>
                </div>
            </ListGroup.Item>
        </ListGroup>
    </Card>
);
 
export default BalanceSummary;