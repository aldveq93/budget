import React from 'react';
import {Alert} from 'react-bootstrap';

const Feedback = ({feedbackType, feedbackMessage, setFeedbackFalse}) => (
    <>
        {
            feedbackType === 'danger' ?
                <Alert variant="danger" onClose={setFeedbackFalse} dismissible>
                    <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                    <p>
                        {feedbackMessage}
                    </p>
                </Alert>    
            :
            feedbackType === 'success' ?

                <Alert variant="success" onClose={setFeedbackFalse} dismissible>
                    <Alert.Heading>How's it going?!</Alert.Heading>
                    <p>
                        {feedbackMessage}
                    </p>
                </Alert>    
            :
            null
        }
    </>
);

export default Feedback;